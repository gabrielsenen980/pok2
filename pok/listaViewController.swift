//
//  UiViewController.swift
//  pok
//
//  Created by COTEMIG on 04/08/22.
//

import UIKit

struct pokemon {
    var nome: String
    var imageURL : String
    var descricao : String
    
}

class listaViewController: UIViewController, UITableViewDataSource {
    var pokemons: [pokemon] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as? MyCell {
        cell.label.text = "Linha atual: \(indexPath.row)"
        return cell
        }
        return UITableViewCell()
        
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var listaPokemon: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
